const { GraphQLScalarType } = require('graphql');
const { Kind } = require('graphql/language');

module.exports = {
  Query: {
    events: async (_, args, { dataSources }) => {
      let { orderBy, order, limit, after } = args;
      if (orderBy === 'category') {
        orderBy = 'categoryId';
      }
      const { hasMore, cursor, events } = await dataSources.eventAPI.getAllEvents({ orderBy, order, limit, after });
      return { hasMore, cursor, events };
    },
  },
  Mutation: {
    createEvent: async (_, args, { dataSources }) => {
      const { input } = args;
      try {
        const event = await dataSources.eventAPI.createEvent(input);
        return {
          code: 0,
          success: true,
          message: 'create event successfully',
          event,
        };
      } catch (err) {
        return {
          code: 1,
          success: false,
          message: err.message,
        };
      }
    },

    deleteEvent: async (_, args, { dataSources }) => {
      const { eventId } = args;
      await dataSources.eventAPI.deleteEvent(eventId);
      return {
        success: true,
        message: `delete event ${eventId} successfully`,
      };
    },

    editEvent: async (_, args, { dataSources }) => {
      const { eventId, input } = args;
      try {
        const event = await dataSources.eventAPI.edit(eventId, input);
        return {
          code: 0,
          success: true,
          message: `edit event ${eventId} successfully`,
          event,
        };
      } catch (err) {
        return {
          code: 1,
          success: false,
          message: err.message,
        };
      }
    }
  },
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value) {
      return new Date(value);
    },
    serialize(value) {
      return value.toISOString();
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.STRING) {
        return new Date(ast.value);
      }
      return null;
    },
  }),
};

