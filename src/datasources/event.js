const { DataSource } = require('apollo-datasource');
const { Op } = require('sequelize');
const logger = require('../logger').child({ label: 'datasources/event' });
const { UserInputError } = require('apollo-server');

class EventAPI extends DataSource {
  constructor({ store }) {
    super();
    this.store = store;
  }

  initialize(config) {
    this.context = config.context;
  }

  async getAllEvents({ orderBy = 'id', order = 'ASC', limit, after }) {
    logger.debug({ query: { orderBy, order, limit, after } }, 'get all events');
    const options = {
      include: [this.store.category],
      order:[[orderBy, order]],
      limit,
    };
    if (orderBy !== 'id') {
      options.order.push(['id', order]);
    }
    if (after) {
      const [afterId, afterOrderBy] = after.split('$$');
      const op = order === 'ASC' ? Op.gt : Op.lt;
      if (orderBy === 'id') {
        options.where = { id: { [op]: afterId } };
      } else {
        options.where = {
          [Op.or]: [
            { [orderBy]: { [op]: afterOrderBy } },
            {
              [Op.and]: [
                { [orderBy]: { [op]: afterOrderBy } },
                { id: { [op]: afterId } },
              ]
            }
          ]
        };
      }
    }
    const result = await this.store.events.findAll(options);
    const events = result.map(e => ({
      ...e.dataValues,
      category: e.category.name
    }));
    const hasMore = result.length > 0;
    let cursor = null;
    if (hasMore) {
      const lastEvent = events[events.length - 1];
      let lastOrderBy = lastEvent[orderBy];
      if (lastOrderBy instanceof Date) {
        lastOrderBy = lastOrderBy.toISOString();
      }
      cursor = lastEvent.id + '$$' + lastOrderBy;}
    return { hasMore, cursor, events };
  }

  async createEvent(input) {
    logger.debug({ input }, 'create an event');
    this.valideInput(input);
    const { category } = input;
    delete input.category;
    const eventCategory = await this.getCategory(category);
    const model = this.store.events.build(input);
    model.categoryId = eventCategory.id;
    const event = await model.save();
    return {
      ...event.dataValues,
      category,
    };
  }

  valideInput(input) {
    const { title, description, startAt, endAt } = input;
    if (title.length > 50) {
      throw new UserInputError('title.length > 50', { invalidArgs: ['title'] });
    }
    if (description && description.length > 1000) {
      throw new UserInputError('description.length > 1000', { invalidArgs: ['description'] });
    }
    if (startAt.getTime() > endAt.getTime()) {
      throw new UserInputError('startAt > endAt', { invalidArgs: ['startAt', 'endAt'] });
    }
  }

  async deleteEvent(eventId) {
    logger.debug({ eventId }, 'delete an event');
    await this.store.events.destroy({ where: { id: eventId } });
  }

  async edit(eventId, form) {
    logger.debug({ eventId, form }, 'edit an event');
    this.valideInput(form);
    const { category } = form;
    const eventCategory = await this.getCategory(category);
    delete form.category;
    await this.store.events.update({
      ...form,
      categoryId: eventCategory.id,
    }, {
      where: { id: eventId },
    });
    const event = await this.store.events.findOne({
      where: { id: eventId },
      include: [this.store.category]
    });
    return {
      ...event.dataValues,
      category: event.category.name,
    };
  }

  async getCategory(name) {
    return await this.store.eventCategories.findOne({
      where: { name }
    });
  }
}

module.exports = EventAPI;
