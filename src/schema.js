const { gql } = require('apollo-server');

const typeDefs = gql`

  type Query {
    events(
      limit: Int = 100
      """ Pass cursor of response to get the result of after the cursor """
      after: String
      orderBy: EventOrderByInput
      order: OrderBy
    ): EventsQueryResponse!
  }

  type Mutation {
    createEvent(input: CreateEventInput): EventCreateResponse!
    editEvent(eventId: ID!, input: EditEventInput): EventEditResponse!
    deleteEvent(eventId: ID!): EventDeleteResponse!
  }

  type EventsQueryResponse {
    cursor: String!
    hasMore: Boolean!
    events: [Event]!
  }

  scalar Date

  type Event {
    id: ID!
    title: String!
    description: String
    startAt: Date!
    endAt: Date!
    category: EventCategory!
    createdAt: Date!
    updatedAt: Date!
  }

  enum EventCategory {
    PERSONAL
    BUSINESS
    OTHER
  }

  enum EventOrderByInput {
    title
    startAt
    endAt
    category
  }

  enum OrderBy {
    ASC
    DESC
  }

  input CreateEventInput {
    title: String!
    description: String
    startAt: Date!
    endAt: Date!
    category: EventCategory!
  }

  type EventCreateResponse {
    code: String!
    success: Boolean!
    message: String!
    event: Event
  }

  input EditEventInput {
    title: String!
    description: String
    startAt: Date!
    endAt: Date!
    category: EventCategory!
  }

  type EventEditResponse {
    code: String!
    success: Boolean!
    message: String!
    event: Event
  }

  type EventDeleteResponse {
    success: Boolean!
    message: String
  }

`;

module.exports = typeDefs;
