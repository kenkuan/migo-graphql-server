const SQL = require('sequelize');
const config = require('./config');

module.exports = {
  createStore,
};

function createStore() {
  const Op = SQL.Op;
  const operatorsAliases = {
    $in: Op.in,
  };

  const options = {
    dialect: 'mysql',
    dialectModule: require('mysql2'),
    dialectOptions: {
      host: config.get('db.host'),
    },
    operatorsAliases,
    logging: false,
  };
  const db = new SQL(config.get('db.name'), config.get('db.user'), config.get('db.password'), options);

  const events = db.define('event', {
    id: {
      type: SQL.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: SQL.STRING,
      allowNull: false,
    },
    description: {
      type: SQL.STRING(1000),
      allowNull: true
    },
    startAt: SQL.DATE,
    endAt: SQL.DATE,
  }, {
    indexes: [
      { fields: ['title'] },
      { fields: ['description'] },
      { fields: ['startAt'] },
      { fields: ['endAt'] },
    ]
  });

  const eventCategories = db.define('event_category', {
    id: {
      type: SQL.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: SQL.STRING,
  });

  const category = events.belongsTo(eventCategories, { as: 'category' });

  return {
    events,
    eventCategories,
    category,
    init: async ({ force = false }) => {
      await db.sync({ force  });
      await Promise.all([
        eventCategories.create({ name: 'PERSONAL' }),
        eventCategories.create({ name: 'BUSINESS' }),
        eventCategories.create({ name: 'OTHER' }),
      ]);
    },
    close: () => db.close(),
  };
}
