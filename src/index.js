const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const { createStore } = require('./store');
const resolvers = require('./resolvers');

const EventAPI = require('./datasources/event');
const store = createStore();
const dataSources = () => ({
  eventAPI: new EventAPI({ store }),
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources,
  introspection: true,
  playground: true,
});

async function start() {
  await store.init();
  const { url } = await server.listen();
  console.log(`🚀 Server ready at ${url}`);
}

if (process.env.NODE_ENV !== 'test') {
  start();
}

module.exports = {
  dataSources,
  typeDefs,
  resolvers,
  ApolloServer,
  EventAPI,
  store,
  server,
};
