var convict = require('convict');

// Define a schema
var config = convict({
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV'
  },
  db: {
    host: {
      doc: 'Database host name/IP',
      format: '*',
      default: '127.0.0.1',
      env: 'DB_HOST'
    },
    name: {
      doc: 'Database name',
      format: String,
      default: 'test',
      env: 'DB_NAME'
    },
    user: {
      doc: 'Database user name',
      format: String,
      default: 'root',
      env: 'DB_USER'
    },
    password: {
      doc: 'Database password',
      format: String,
      default: 'test',
      env: 'DB_PASSWORD'
    },
    port: {
      doc: 'Database port',
      format: Number,
      default: 3306,
      env: 'DB_PORT'
    }
  }
});

config.validate({ allowed: 'strict' });

module.exports = config;
