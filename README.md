# Get Start

This API server is built by Apollo GraphQL https://www.apollographql.com

To start developing on your machine:

## Prerequisite
1. Install nvm `https://github.com/nvm-sh/nvm#install--update-script`
1. Install docker on mac / linux

## Run the server on local
1. `git clone git@gitlab.com:kenkuan/migo-graphql-server.git`
1. Start local mysql server `docker-compose up -d`
1. `nvm use`
1. `npm i`
1. `npm start`
1. Open `https://localhost:4000`

## Dev Flow

We use git-flow

1. Branch from `develop`  with name `feature/xxx`, `fix/xxx`
1. `npm run test:watch`
1. Writing a new test for the new feature.
1. Implement the feature to make the test passed.
1. Create a merge request(MR) to `develop`
1. Get someone to review the MR.

## Code Coverage

We use codecov(https://codecov.io) to collect code coverage report.

The code coverage report is at https://codecov.io/gl/kenkuan/migo-graphql-server

## Deployment
We use now(https://zeit.co/now) to deploy test server per push and beta environment.

Beta server is aligned with `develop` branch.

You can acess the beta server here https://migo-graphql-server.kenkuan.now.sh/

# Todo
- [x] Setup code coverage - with jest & codecov
- [x] Setup logger - with pino
- [x] Setup beta server - with now
- [x] Create commit hook - with husky
- [x] Introduce `.env` for config - use convict for now
- [x] Replace sqlite3 with production database, Postgre, MySQL etc.
- [ ] Use migration to manage database schema.
- [ ] Dockerize the app
