const { createTestClient } = require('apollo-server-testing');
const gql = require('graphql-tag');

const { constructTestServer } = require('./__utils');

describe('Mutation', () => {

  let query;
  let mutate;
  let store;

  beforeEach(async () => {
    const { server, store: theStore } = constructTestServer();
    store = theStore;
    await store.init({ force: true });
    const testClient = createTestClient(server);
    query = testClient.query;
    mutate = testClient.mutate;
  });

  describe('create', () => {

    const CREATE = gql`
      mutation createEvent($input: CreateEventInput) {
        createEvent(input: $input) {
          code
          success
          message
          event {
            title
            description
            startAt
            endAt
            category
          }
        }
      }
    `;

    it('should create a new event', async () => {
      const input = {
        title: 'foo',
        description: 'bar',
        startAt: new Date(10000),
        endAt: new Date(20000),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      expect(createRes.data.createEvent.success).toBeTruthy();

      const GET_EVENTS = gql`
        query {
          events {
            events {
              title
              description
              startAt
              endAt
              category
            }
          }
        }
      `;

      const res = await query({ query: GET_EVENTS });

      expect(res.data.events.events[0]).toEqual(createRes.data.createEvent.event);
    });

    it('should create a new event without description', async () => {
      const input = {
        title: 'foo',
        startAt: new Date(10000),
        endAt: new Date(20000),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      expect(createRes.data.createEvent.success).toBeTruthy();

      const GET_EVENTS = gql`
        query {
          events {
            events {
              title
              description
              startAt
              endAt
              category
            }
          }
        }
      `;

      const res = await query({ query: GET_EVENTS });

      expect(res.data.events.events[0]).toEqual(createRes.data.createEvent.event);
    });

    it('should failed to create a new event if title.length > 50', async () => {
      let title = '';
      for (var i = 0; i <= 51; i++) {
        title += i;
      }
      const input = {
        title,
        description: 'bar',
        startAt: new Date(10000),
        endAt: new Date(20000),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      expect(createRes).toMatchSnapshot();
    });

    it('should failed to create a new event if description.length > 1000', async () => {
      let description = 'x'.repeat(1001);
      const input = {
        title: 'foo',
        description,
        startAt: new Date(10000),
        endAt: new Date(20000),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      expect(createRes).toMatchSnapshot();
    });

    it('should create a new event if description.length = 1000', async () => {
      const description = 'x'.repeat(1000);
      const input = {
        title: 'foo',
        description,
        startAt: new Date(10000),
        endAt: new Date(20000),
        category: 'OTHER',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      expect(createRes.data.createEvent.success).toBeTruthy();

      const GET_EVENTS = gql`
        query {
          events {
            events {
              title
              description
              startAt
              endAt
              category
            }
          }
        }
      `;

      const res = await query({ query: GET_EVENTS });

      expect(res.data.events.events[0]).toEqual(createRes.data.createEvent.event);
    });

    it('should failed to create a new event if startAt is not date', async () => {
      const input = {
        title: 'foo',
        description: 'bar',
        startAt: 'bbb',
        endAt: new Date(20000),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      expect(createRes).toMatchSnapshot();
    });

    it('should failed to create a new event if startAt > endAt', async () => {
      const input = {
        title: 'foo',
        description: 'bar',
        startAt: new Date(1000),
        endAt: new Date(0),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      expect(createRes).toMatchSnapshot();
    });

  });

  describe('delete', () => {
    it('should create and delete the event', async () => {

      const CREATE = gql`
        mutation createEvent($input: CreateEventInput) {
          createEvent(input: $input) {
            event {
              id
            }
          }
        }
      `;

      const input = {
        title: 'foo',
        description: 'bar',
        startAt: new Date(10000),
        endAt: new Date(20000),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      const DELETE = gql`
        mutation deleteEvent($eventId: ID!) {
          deleteEvent(eventId: $eventId) {
            success
            message
          }
        }
      `;

      const res = await query({
        query: DELETE,
        variables: {
          eventId: createRes.data.createEvent.event.id
        }
      });

      expect(res).toMatchSnapshot();

      const count = await store.events.count();
      expect(count).toBe(0);
    });

  });

  describe('edit', () => {

    let eventId;

    beforeEach(async () => {

      const CREATE = gql`
        mutation createEvent($input: CreateEventInput) {
          createEvent(input: $input) {
            event {
              id
            }
          }
        }
      `;

      const input = {
        title: 'foo',
        description: 'bar',
        startAt: new Date(10000),
        endAt: new Date(20000),
        category: 'PERSONAL',
      };

      const createRes = await mutate({
        mutation: CREATE,
        variables: { input }
      });

      eventId = createRes.data.createEvent.event.id;
    });

    const EDIT = gql`
      mutation deleteEvent($eventId: ID!, $input: EditEventInput) {
        editEvent(eventId: $eventId, input: $input) {
          code
          success
          message
          event {
            title
            description
            startAt
            endAt
            category
          }
        }
      }
    `;

    it('should edit an event', async () => {
      const editForm = {
        title: 'nice',
        description: 'gg',
        startAt: new Date(2000),
        endAt: new Date(3000),
        category: 'BUSINESS'
      };
      const res = await query({
        query: EDIT,
        variables: {
          eventId,
          input: editForm
        }
      });

      expect(res).toMatchSnapshot();
    });

    it('should edit an event without description', async () => {
      const editForm = {
        title: 'nice',
        startAt: new Date(2000),
        endAt: new Date(3000),
        category: 'BUSINESS'
      };
      const res = await query({
        query: EDIT,
        variables: {
          eventId,
          input: editForm
        }
      });

      expect(res.data.editEvent.success).toBeTruthy();
      expect(res).toMatchSnapshot();
    });

    it('should failed to edit an event if title.length > 50', async () => {
      let title = 'x'.repeat(51);
      const editForm = {
        title,
        description: 'gg',
        startAt: new Date(2000),
        endAt: new Date(3000),
        category: 'BUSINESS'
      };
      const res = await query({
        query: EDIT,
        variables: {
          eventId,
          input: editForm
        }
      });

      expect(res).toMatchSnapshot();
    });

    it('should failed to edit an event if description.length > 1000', async () => {
      let description = 'x'.repeat(1001);
      const editForm = {
        title: 'ha',
        description,
        startAt: new Date(2000),
        endAt: new Date(3000),
        category: 'BUSINESS'
      };
      const res = await query({
        query: EDIT,
        variables: {
          eventId,
          input: editForm
        }
      });

      expect(res).toMatchSnapshot();
    });

    it('should failed to edit an event if startAt > endAt', async () => {
      const editForm = {
        title: 'ha',
        description: 'gg',
        startAt: new Date(4000),
        endAt: new Date(3000),
        category: 'BUSINESS'
      };
      const res = await query({
        query: EDIT,
        variables: {
          eventId,
          input: editForm
        }
      });

      expect(res.data.editEvent.success).toBeFalsy();
      expect(res).toMatchSnapshot();
    });
  });

});
