
const {
  context: defaultContext,
  typeDefs,
  resolvers,
  ApolloServer,
  EventAPI,
  store,
} = require('../src');

/**
 * Integration testing utils
 */
const constructTestServer = ({ context = defaultContext } = {}) => {
  const eventAPI = new EventAPI({ store });

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => ({ eventAPI }),
    context,
    resolverValidationOptions: {
      requireResolversForResolveType: false
    },
  });

  return { server, eventAPI, store };
};

module.exports.constructTestServer = constructTestServer;

afterAll(async () => {
  await store.close();
});
