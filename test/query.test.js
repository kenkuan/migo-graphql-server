const { createTestClient } = require('apollo-server-testing');
const gql = require('graphql-tag');

const { constructTestServer } = require('./__utils');

describe('Queries', () => {

  let query;
  let store;

  beforeEach(async () => {
    const { server, store: theStore } = constructTestServer();
    store = theStore;
    await store.init({ force: true });
    query = createTestClient(server).query;
  });

  it('should fetches list of events', async () => {
    await store.events.build({
      title: 'test',
      startAt: new Date(1000),
      endAt: new Date(2000),
      categoryId: 1,
      createdAt: new Date(1),
      updatedAt: new Date(2),
    }).save();

    const GET_EVENTS = gql`
      query {
        events {
          events {
            title
            description
            startAt
            endAt
            category
          }
        }
      }
    `;

    const res = await query({ query: GET_EVENTS });

    expect(res).toMatchSnapshot();
  });

  const GET_EVENTS_ORDER_BY = gql`
    query events($orderBy: EventOrderByInput, $order: OrderBy, $after: String, $limit: Int) {
      events(orderBy: $orderBy, order: $order, limit: $limit, after: $after) {
        events {
          title
        }
      }
    }
  `;

  it('should order events by startAt ASC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(100000 - i * 1000),
        endAt: new Date(1000000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY,
      variables: {
        orderBy: 'startAt',
        order: 'ASC',
      }
    });

    expect(res).toMatchSnapshot();
  });

  it('should order events by startAt DESC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(100000 - i * 1000),
        endAt: new Date(1000000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY,
      variables: {
        orderBy: 'startAt',
        order: 'DESC',
      }
    });

    expect(res).toMatchSnapshot();
  });

  it('should order events by title ASC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(2000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY,
      variables: {
        orderBy: 'title',
        order: 'ASC',
      }
    });

    expect(res).toMatchSnapshot();
  });

  it('should order events by title DESC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000 - i),
        endAt: new Date(2000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY,
      variables: {
        orderBy: 'title',
        order: 'DESC',
      }
    });

    expect(res).toMatchSnapshot();
  });

  it('should order events by endAt ASC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY,
      variables: {
        orderBy: 'endAt',
        order: 'ASC',
      }
    });

    expect(res).toMatchSnapshot();
  });

  it('should order events by endAt DESC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY,
      variables: {
        orderBy: 'endAt',
        order: 'DESC',
      }
    });

    expect(res).toMatchSnapshot();
  });

  it('should order events by category DESC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: (i % 3) + 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY,
      variables: {
        orderBy: 'category',
        order: 'DESC',
      }
    });

    expect(res).toMatchSnapshot();
  });

  const GET_EVENTS_ORDER_BY_LIMIT = gql`
    query events($orderBy: EventOrderByInput, $order: OrderBy, $after: String, $limit: Int) {
      events(orderBy: $orderBy, order: $order, limit: $limit, after: $after) {
        hasMore
        cursor
        events {
          title
        }
      }
    }
  `;

  it('should fetch first 5 events orderBy endAt ASC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const res = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        orderBy: 'endAt',
        order: 'ASC',
        limit: 5,
      },
    });

    expect(res).toMatchSnapshot();
  });

  it('should fetch next 5 events order by endAt ASC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const firstRes = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        orderBy: 'endAt',
        order: 'ASC',
        limit: 5,
      },
    });

    expect(firstRes).toMatchSnapshot();

    const res = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        orderBy: 'endAt',
        order: 'ASC',
        limit: 5,
        after: firstRes.data.events.cursor,
      },
    });

    expect(res).toMatchSnapshot();
  });

  it('should fetch next 5 events order by endAt DESC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const firstRes = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        orderBy: 'endAt',
        order: 'DESC',
        limit: 5,
      },
    });

    expect(firstRes).toMatchSnapshot();

    const res = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        orderBy: 'endAt',
        order: 'DESC',
        limit: 5,
        after: firstRes.data.events.cursor,
      },
    });

    expect(res).toMatchSnapshot();
  });

  it('should fetch next 5 events order by id ASC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const firstRes = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        limit: 5,
      },
    });

    expect(firstRes).toMatchSnapshot();

    const res = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        limit: 5,
        after: firstRes.data.events.cursor,
      },
    });

    expect(res).toMatchSnapshot();
  });

  it('should fetch next 5 events order by title DESC', async () => {
    for (let i = 0; i < 10; i++) {
      await store.events.build({
        title: 'test' + i,
        startAt: new Date(1000),
        endAt: new Date(100000 - i * 1000),
        categoryId: 1,
        createdAt: new Date(1),
        updatedAt: new Date(2),
      }).save();
    }

    const firstRes = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        orderBy: 'title',
        order: 'DESC',
        limit: 5,
      },
    });
    expect(firstRes).toMatchSnapshot();

    const res = await query({
      query: GET_EVENTS_ORDER_BY_LIMIT,
      variables: {
        orderBy: 'title',
        order: 'DESC',
        limit: 5,
        after: firstRes.data.events.cursor,
      },
    });

    expect(res).toMatchSnapshot();
  });
});
